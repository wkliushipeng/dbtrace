package john.walker.log;

public interface ILog {

	public static final int DEBUG = 0x01;
	public static final int INFO  = 0x02;
	public static final int WARN  = 0x03;
	public static final int ERROR = 0x04;
	public static final int FATAL = 0x05;

	public void debug(String msg) ;

	public void info(String msg) ;

	public void warn(String msg) ;

	public void error(String msg) ;

	public void fatal(String msg) ;

	public void log(String msg);

	public void newLine();

	public int getLevel();

	public void setLevel(int level);
}
